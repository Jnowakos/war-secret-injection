# War Secret Injection

Using Secrets with  `subPath: customerapp.properties`  
Based on this:
https://www.patrickdap.com/post/mounting-secrets-configmaps-without-deleting/

# Sample Secret:
`kind: Secret
apiVersion: v1
metadata:
  name: test-important-secrets
  namespace: test
data:
  customerapp.properties: RU5WX1ZBTD01Njc4
type: Opaque`


# Sample Deployment:
`kind: Deployment
apiVersion: apps/v1
metadata:
  annotations:
  name: war-secret-injection2
  generation: 9
  namespace: test
  labels:
    app: war-secret-injection2
    app.kubernetes.io/component: war-secret-injection2
    app.kubernetes.io/instance: war-secret-injection2
    app.kubernetes.io/name: war-secret-injection2
    app.kubernetes.io/part-of: war-secret-injection-app
    app.openshift.io/runtime: spring-boot
    app.openshift.io/runtime-namespace: test
spec:
  replicas: 1
  selector:
    matchLabels:
      app: war-secret-injection2
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: war-secret-injection2
        deployment: war-secret-injection2
      annotations:
        openshift.io/generated-by: OpenShiftWebConsole
    spec:
      volumes:
        - name: test-important-secrets
          secret:
            secretName: test-important-secrets
            defaultMode: 420
      containers:
        - name: war-secret-injection2
          image: >-
            image-registry.openshift-image-registry.svc:5000/test/war-secret-injection2@sha256:bd475d6cccfd432b8be1ca4228e52b8c5be1d85e37a02c5b2e2e6c325a78e660
          ports:
            - containerPort: 8080
              protocol: TCP
            - containerPort: 8443
              protocol: TCP
          resources: {}
          volumeMounts:
            - name: test-important-secrets
              readOnly: true
              mountPath: /usr/src/app/WEB-INF/classes/customerapp.properties
              subPath: customerapp.properties
          terminationMessagePath: /dev/termination-log
          terminationMessagePolicy: File
          imagePullPolicy: Always`