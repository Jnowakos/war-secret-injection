package com.jason.test.warsecretinjection;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:customerapp.properties")
public class LoadConfig {
    
}
