package com.jason.test.warsecretinjection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class SampleController {

    @Value("${ENV_VAL}")
    private Integer value;

    @GetMapping()
    public Integer getSecrets() {
        return value;
    }
}
