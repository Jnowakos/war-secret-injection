package com.jason.test.warsecretinjection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WarSecretInjectionApplication {

	public static void main(String[] args) {
		SpringApplication.run(WarSecretInjectionApplication.class, args);
	}

}
