FROM registry.access.redhat.com/ubi9/openjdk-17:1.15-2
WORKDIR /usr/src/app
USER 0
COPY ./target/war-secret-injection-0.0.1-SNAPSHOT.war /usr/src/app

RUN chown -R 1001:0 /usr/src/app && \
    chmod -R g=u  /usr/src/app && \
    jar xvf war-secret-injection-0.0.1-SNAPSHOT.war && \
    rm ./war-secret-injection-0.0.1-SNAPSHOT.war && \
    chown -R 1001:0 /usr/src/app && \
    chmod -R g=u  /usr/src/app 
    
USER 1001
ENV PORT 8080
EXPOSE $PORT
#CMD [ "java", "-jar", "war-secret-injection-0.0.1-SNAPSHOT.war" ]
CMD [ "java", "org.springframework.boot.loader.WarLauncher" ]